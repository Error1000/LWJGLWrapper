//Set the current version so that opengl knows which version of the compiler to use to compile this
#version 120

//Note this time we don't get the vertices attribute since when we give the data we only give it to the vertex shader

//But what we do need to make is the uniform that we will use
uniform sampler2D texture;
uniform float useColor;
uniform float r,g,b;


//Get the pixel_coord variable from the vertex shader
varying vec2 pixel_coord;

void main() {
   //Set the texture of the quad
    if(useColor == 1){
    gl_FragColor = texture2D(texture, pixel_coord) *  vec4( r/256, g/256, b/256, 1);
	}else{
	gl_FragColor = texture2D(texture, pixel_coord);
	}
}

