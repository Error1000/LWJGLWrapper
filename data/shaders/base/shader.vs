//Set the current version so that opengl knows which version of the compiler to use to compile this
#version 120

//Get the variables from the Shader class that compiled it
//Note: it must have the same name as the name placed in the Shader class
attribute vec4 vertices;
attribute vec2 tex_coord;

//Make a varying variable that the fragment shader can access so it can get the texture coords
varying vec2 pixel_coord;

//Make a uniform for other to set
uniform mat4 projection;

void main() {
   pixel_coord = tex_coord;
   gl_Position = projection * vertices;
   
}