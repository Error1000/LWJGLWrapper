package examples.tictactoe;


import org.joml.Vector2f;

import mainWrapper.renderEgnine.Material.Shader;
import mainWrapper.renderEgnine.Objects.DrawableObject;
import mainWrapper.renderEgnine.Shape.RectShape;
import mainWrapper.renderEgnine.Shape.Shape;


public class Grid extends DrawableObject {

	private DrawableObject[][] objects;
	private DrawableObject[] drawableGridH;
	private DrawableObject[] drawableGridW;

	private int gridW, gridH, cellW, cellH;

	public Grid(int gridW, int gridH, int squareW, int squareH, int lineW, int lineH, Shader gridShader) {
		super(new RectShape(), gridShader);

		this.cellW = squareW;
		this.cellH = squareH;
		this.gridH = gridH;
		this.gridW = gridW;

		int test;

		setSize(gridW * squareW, gridH * squareH);

		objects = new DrawableObject[gridH][gridW];
		drawableGridH = new DrawableObject[gridH + 1];
		drawableGridW = new DrawableObject[gridW + 1];

		if ((gridH + 1) % 2 == 0)
			test = squareH / 2;
		else
			test = 0;

		for (int i = 0; i < (gridH + 1) / 2 + (((gridH + 1) % 2 == 0) ? 0 : 1); i++) {
			Shape shape = new RectShape();

			shape.setSize(getSize().x + lineH, lineH);

			drawableGridH[i] = new DrawableObject(shape, getShader());
			drawableGridH[i].setPosition(0, i * squareH + test);

			Shape shape2 = new RectShape();
			shape2.setSize(getSize().x + lineH, lineH);
			drawableGridH[i + (gridH + 1) / 2] = new DrawableObject(shape2, getShader());
			drawableGridH[i + (gridH + 1) / 2].setPosition(0, -(i * squareH + test));
		}

		if ((gridW + 1) % 2 == 0)
			test = squareW / 2;
		else
			test = 0;
		for (int i = 0; i < (gridW + 1) / 2 + (((gridW + 1) % 2 == 0) ? 0 : 1); i++) {
			Shape shape = new RectShape();

			shape.setSize(lineW, getSize().y + lineW);

			drawableGridW[i] = new DrawableObject(shape, getShader());
			drawableGridW[i].setPosition(i * squareW + test, 0);

			Shape shape2 = new RectShape();
			shape2.setSize(lineW, getSize().y + lineW);
			drawableGridW[i + (gridW + 1) / 2] = new DrawableObject(shape2, getShader());
			drawableGridW[i + (gridW + 1) / 2].setPosition(-(i * squareW + test), 0);
		}

	}

	@Override
	public void render() {
		for (DrawableObject[] line : objects) {
			for (DrawableObject o : line) {
				if (o != null)
					o.render();
			}
		}

		for (DrawableObject o : drawableGridH) {
			if (o != null)
				o.render();

		}

		for (DrawableObject o : drawableGridW) {
			if (o != null)
				o.render();

		}
	}

	@Override
	public void setPosition(float x, float y) {

		pos = new Vector2f(x, y);

		int test = 0;
		if ((gridH + 1) % 2 == 0)
			test = cellH / 2;
		else
			test = 0;

		for (int i = 0; i < (gridH + 1) / 2 + (((gridH + 1) % 2 == 0) ? 0 : 1); i++) {

			drawableGridH[i].setPosition(0 + getPosition().x, (i * cellH + test) + getPosition().y);

			drawableGridH[i + (gridH + 1) / 2].setPosition(0 + getPosition().x,
					-(i * cellH + test) + getPosition().y);

		}

		if ((gridW + 1) % 2 == 0)
			test = cellW / 2;
		else
			test = 0;
		for (int i = 0; i < (gridW + 1) / 2 + (((gridW + 1) % 2 == 0) ? 0 : 1); i++) {

			drawableGridW[i].setPosition((i * cellW + test) + getPosition().x, 0 + getPosition().y);

			drawableGridW[i + (gridW + 1) / 2].setPosition(-(i * cellW + test) + getPosition().x,
					0 + getPosition().y);

		}

		for (int yGrid = 0; yGrid < gridH; yGrid++) {
			for (int xGrid = 0; xGrid < gridW; xGrid++) {
				
				if (objects[yGrid][xGrid] != null) {
					 objects[yGrid][xGrid].setPosition( (((xGrid) * cellW + cellW / 2) - getSize().x / 2) + getPosition().x,(((yGrid) * cellH + cellH / 2) - getSize().y / 2) + getPosition().y);
				}
				
			}
		}

		
	}




	public void addObjectToGrid(int x, int y, DrawableObject o) {
		o.setPosition(((x * cellW + cellW / 2) - getSize().x / 2) + getPosition().x,((y * cellH + cellH / 2) - getSize().y / 2) + getPosition().y);
		objects[y][x] = o;

	}
	
	public void setObjectOnGrid(int x, int y, DrawableObject o) {
		o.setPosition(((x * cellW + cellW / 2) - getSize().x / 2) + getPosition().x,
				((y * cellH + cellH / 2) - getSize().y / 2) + getPosition().y);
		objects[y][x] = o;

	}

	public DrawableObject getObjectFromGrid(int x, int y) {
		return objects[y][x];
	}

	public int[] onWihichSquareIsLocation(Vector2f loc) {
		int[] squarePos = new int[2];
		squarePos[0] = -1;
		squarePos[1] = -1;
		for (int yGrid = 0; yGrid < gridH; yGrid++) {
			for (int xGrid = 0; xGrid < gridW; xGrid++) {
				Vector2f squareCenter = new Vector2f((( xGrid * cellW + cellW / 2) - getSize().x / 2) + getPosition().x,(( yGrid * cellH + cellH / 2) - getSize().y / 2) + getPosition().y);
				if (loc.x > (squareCenter.x - cellW / 2) && loc.x < (squareCenter.x + cellW / 2)) {
					if (loc.y > (squareCenter.y - cellH / 2) && loc.y < (squareCenter.y + cellH / 2)) {
						squarePos[0] = xGrid;
						squarePos[1] = yGrid;
					}
				}

			}
		}

		return squarePos;
	}


	public int getCellW() {
		return cellW;
	}

	public int getCellH() {
		return cellH;
	}

}
