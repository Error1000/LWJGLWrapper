package examples.tictactoe;

public class PublicVariables {

	public static final float[] xShapeVertices = new float[] {
		//Note: the triangle vertices are not replicated because indices so a triangle can have 1 or maximum 3 vertices and then we can use indices to tell opnelg in which order to draw the vertices so the one vertex triangle, can be drawn if instead of drawing just one vertex we use others for other triangles to form this one
			//First rectangle
			 //First triangle
			   -0.9f, 1f,
			   -1f, 0.9f,
			   0.9f, -1f,
			 //Second triangle
			   1f, -0.9f,
			   
		    //Second rectangle
			 //First triangle  
			   0.9f, 1f,
			   1f, 0.9f,
			   -1f, -0.9f,
			   
			 //Second triangle
			   -0.9f, -1f
	};
	
	public static final float[] xShapeTextureCoords = new float[] {
	};
	
	public static final int[] xShapeIndices = new int[] {
		//Note: This is the array that lists in which order should the vertices be drawn
		 //First rectangle
		   //First triangle
			0,
			1,
			2,
			
		   //Second triangle
			3,
			2,
			0,
			
		//Second rectangle
		   //First triangle
			4,
			5,
			6,
			
		   //Second triangle
			6,
			7,
			5
			
	};
	
	
	
	
	//----------------------------------------------------------------------------------
	
	

	public static final float[] oShapeVertices = new float[] {
		//Note: the triangle vertices are not replicated because indices so a triangle can have 1 or maximum 3 vertices and then we can use indices to tell opnelg in which order to draw the vertices so the one vertex triangle, can be drawn if instead of drawing just one vertex we use others for other triangles to form this one
			//First rectangle
			  //First triangle
			    -0.7f, 1f, 
			    -0.7f, 0.9f, 
			     0.7f, 0.9f, 
			  //Second triangle
			     0.7f, 1f,
			     
		    //Second rectangle
			  //First triangle
			    -0.6f, 0.9f, 
			    -0.6f, -0.9f, 
			  //Second triangle
			    -0.7f, -0.9f, 
			     
		    //Third rectangle
			  //First triangle
			   -0.7f, -1f, 
			    0.7f, -1f, 
			  //Second triangle
			    0.7f, -0.9f, 
			     
		    //Fourth rectangle
			  //First triangle
			    0.6f, 0.9f, 
			  //Second triangle
			    0.6f, -0.9f, 
			    
			//Fifth rectangle
			  //First triangle
			    -0.6f, -1f, 
			  //Second triangle
			    0.6f, 1f
	};
	
	public static final float[] oShapeTextureCoords = new float[] {
	};
	
	public static final int[] oShapeIndices = new int[] {
		//Note: This is the array that lists in which order should the vertices be drawn
			//First rectangle
			  //First triangle
			    0,
			    1,
			    2,
			  
			  //Second triangle
			    3,
			    2,
			    0,
			    
			    
			//Second rectangle
			  //First triangle
			    1,
			    4,
			    5,
			  
			  //Second triangle
			    5,
			    6,
			    1,	
			    
			    
		    //Third rectangle
			  //First triangle
			    6,
			    7,
			    8,
			    
			  //Second triangle
			    9,
			    8,
			    6,
			    
			//Fourth rectangle
			  //First triangle
			    10,
			    2,
			    9,
			    
			  //Second triangle
			    11,
			    9,
			    10,
			    
			    
			//Fifth rectangle
			  //First triangle
			    6,
			    12,
			    2,
			  //Second triangle
			    2,
			    13,
			    6
			    
	};



		
	
}
