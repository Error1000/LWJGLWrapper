package examples.tictactoe;


import org.joml.Vector2f;

import mainWrapper.Game;
import mainWrapper.wrapper.Window.Window;
import mainWrapper.renderEgnine.Material.Shader;
import mainWrapper.renderEgnine.Objects.DrawableObject;
import mainWrapper.renderEgnine.Shape.Shape;

public class Main extends Game {

	private static Shader gridShader;

	private static Grid dg;


	// Note true = X and false = 0
	private static boolean XorO = true;
    private static final int INIT_WINDOW_WIDTH = 400;
    private static final int INIT_WINDOW_HEIGHT = 400;
    
    private static int[][] grid;
	public static final void main(String args[]) {
		Window.initWindow(INIT_WINDOW_WIDTH, INIT_WINDOW_HEIGHT, "The tic-tac-toe game!");
		
		startWrapper();
	}


	@Override
	public void setup() {
		backgroundColor(256);

		gridShader = new Shader(readTextFile("data/shaders/base/shader.vs"), readTextFile("data/shaders/base/shader.fs"));
		int gridW = 3;
		int gridH = 3;
		
		int lineW = 10;
		int lineH = 10;
		
		grid = new int[gridH][gridW];
		dg = new Grid(gridW,gridH, (INIT_WINDOW_WIDTH/gridW),(INIT_WINDOW_HEIGHT/gridH), lineW, lineH, gridShader);

	}
	
	
	private static int waitBeforeEndToDraw = 4;

	@Override
	public void render() {

		//System.out.println(Window.getFPS() + " fps!");
		dg.render();

		int whoWon = checkGrid();

		if (whoWon != -1) {
			waitBeforeEndToDraw--;
			if (waitBeforeEndToDraw == 0) {
				switch (whoWon) {
				case 0:
					// System.out.println("None won!");
					Window.setTitle("None won!");
					break;

				case 1:
					// System.out.println("0 won!");
					Window.setTitle("0 won!");
					break;

				case 2:
					// System.out.println("X won!");
					Window.setTitle("X won!");
					break;
					
				default:
					Window.setTitle("None won?");
				}

				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				exit();

			}

		}

	}




	
	@Override
	public void mousePressed(int keyCode, float x, float y) {
	   
		int[] mousePos = dg.onWihichSquareIsLocation(new Vector2f(x, y));
        if(mousePos[0] == -1 || mousePos[1] == -1)return;
        
        if(grid[mousePos[0]][mousePos[1]] > 0)return;
        
		if(XorO) {
		Shape oShape = new Shape(PublicVariables.oShapeVertices, PublicVariables.oShapeTextureCoords,
				PublicVariables.oShapeIndices);
		//We use -40 so we don't make the shape as big as the cell/square
		oShape.setSize(dg.getCellW()-40, dg.getCellH()-40);
		DrawableObject oObj = new DrawableObject(oShape, gridShader);
		dg.addObjectToGrid(mousePos[0], mousePos[1], oObj);
		grid[mousePos[0]][mousePos[1]] = 1;
		
		}else{
		Shape xShape = new Shape(PublicVariables.xShapeVertices, PublicVariables.xShapeTextureCoords,
				PublicVariables.xShapeIndices);
		//We use -40 so we don't make the shape as big as the cell/square
		xShape.setSize(dg.getCellW()-40, dg.getCellH()-40);
		DrawableObject xObj = new DrawableObject(xShape, gridShader);
		dg.addObjectToGrid(mousePos[0], mousePos[1], xObj);
		grid[mousePos[0]][mousePos[1]] = 2;
		
		}
		
		
		XorO = !XorO;
		if (XorO)
			Window.setTitle("X's round!");
		else
			Window.setTitle("0's round!");

	}
	
	
	
	public final static int checkGrid() {
		int whoWon = -1;
		
		//Check each player ( 1= x, 2= 0)
		for (int i = 1; i < 3; i++) {
			
			//Check each 3 rows and columns
			for (int j = 0; j < 3; j++) {
				//Horizontal check
				if (grid[0][j] == i && grid[1][j] == i && grid[2][j] == i)whoWon = i;

				// Vertical check
				if (grid[j][0] == i && grid[j][1] == i && grid[j][2] == i)whoWon = i;

			}

			// Diagonals
			if (grid[0][0] == i && grid[1][1] == i && grid[2][2] == i)whoWon = i;

			if (grid[2][0] == i && grid[1][1] == i && grid[0][2] == i)whoWon = i;
			
		}
		

		if (whoWon == -1) {
			boolean tableFull = false;
			boolean loop = true;
			
			for (int j = 0; j < 3; j++){
				
				for (int k = 0; k < 3; k++){
					
					if(grid[j][k] != 0) {
					   tableFull = true;
					}else{
						tableFull = false;
						loop = false;
					}
					
					if (!loop)break;
				}
				
				if (!loop)break;
			}

			if(tableFull)whoWon = 0;
			
		}

		return whoWon;
	}


}
