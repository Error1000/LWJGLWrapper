package tester.main;

import static tester.common.PublicVariables.*;


import java.awt.FontFormatException;
import java.io.IOException;

import org.joml.Vector2f;

import static org.lwjgl.glfw.GLFW.*;

import mainWrapper.Game;
import mainWrapper.wrapper.Window.Camera;
import mainWrapper.wrapper.Window.Window;
import mainWrapper.renderEgnine.Material.Animation;
import mainWrapper.renderEgnine.Material.Shader;
import mainWrapper.renderEgnine.Material.Texture;
import mainWrapper.renderEgnine.Objects.AnimatedObject;
import mainWrapper.renderEgnine.Objects.DrawableObject;
import mainWrapper.renderEgnine.Objects.TexturedObject;
import mainWrapper.renderEgnine.Shape.RectShape;
import mainWrapper.renderEgnine.Shape.Shape;
import mainWrapper.renderEgnine.UI.Fonts.UIFont;
import mainWrapper.renderEgnine.UI.UIElements.UILabel;

public class Main extends Game {

	private static Shape objectShape;
	private static Shape object2Shape;

	private static Shader objectShader;
	private static Shader object2Shader;

	private static DrawableObject object;
	private static DrawableObject object2;

	private static Animation coolcircle;
	private static Texture stickman;
	
	private static Shader fontShader;
	private static UIFont consolasFont;
    private static UILabel testLabel;
    private static UILabel loadingLabel;

	public static void main(String args[]) {

		Window.initWindow(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_NAME);
		Window.startWindowFullscreen(FULLSCREEN);
		startWrapper();

	}

	@Override
	public void setup() {
		backgroundColor(255);
		fontShader = new Shader(readTextFile("data/shaders/base/shader.vs"), readTextFile("data/shaders/base/shader.fs"));
		
		try {
			consolasFont = new UIFont( "data/resources/Consolas.ttf", fontShader, true);
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
        loadingLabel = new UILabel("Loading...", consolasFont, new Vector2f(0,0));
		
		Window.startUpdateingWindow();

		fontShader.bindShader();
		fontShader.setUniform("useColor", 1f);
		fontShader.setUniform("r", 0f);
		fontShader.setUniform("g", 0f);
		fontShader.setUniform("b", 0f);
        loadingLabel.render();
		Window.stopUpdateingWindow();
		
		
		//Initialize the shape
		objectShape = new RectShape();
		object2Shape = new RectShape();


		
		objectShape.setSize(200, 200);
		object2Shape.setSize(200, 200);


		//Load shader
		objectShader = new Shader(readTextFile("data/shaders/base/shader.vs"), readTextFile("data/shaders/base/shader.fs"));
		object2Shader = new Shader(readTextFile("data/shaders/base/shader.vs"), readTextFile("data/shaders/base/shader.fs"));
        

       
        //Load animation
		coolcircle = new Animation(5, 3, "data/resources/anim/circle/");
		stickman = new Texture("data/resources/textures/stickman.png");


		
		//Make object
		object = new AnimatedObject(objectShape, coolcircle, objectShader);
		object2 = new TexturedObject(object2Shape, stickman, object2Shader);

		
		testLabel = new UILabel("0 fps", consolasFont);
		testLabel.setPosition(-(Window.getWidth()/2-testLabel.getTextWidth()/2), Window.getHeight()/2-testLabel.getTextHeight()/2);
		
        
		Camera.setFollowingObject(object2);
	
	}

	private static int i = 0;

	@Override
	public void render() {


 
		if (i > 400) {
			//System.out.println(Window.getFPS());
			i = 0;
			int oldTextWidth = testLabel.getTextWidth();
			int oldTextHeight = testLabel.getTextHeight();
			
			testLabel.setContent( String.valueOf(Window.getFPS()) + " fps");
			
			//This setPosition will adjust the text position to remain on screen
			testLabel.setPosition( testLabel.getPosition().x +Math.abs(testLabel.getTextWidth() - oldTextWidth)/2, testLabel.getPosition().y+Math.abs(testLabel.getTextHeight() - oldTextHeight)/2 );
			
		}


		//Show object
		object.render();
		object2.render();

		
		fontShader.bindShader();
		fontShader.setUniform("useColor", 1f);
		fontShader.setUniform("r", 256f);
		fontShader.setUniform("g", 0f);
		fontShader.setUniform("b", 0f);
        testLabel.render();
		
		i++;
	}

	@Override
	public void keyIsDown(int keyCode) {

		//Move object when key pressed
		//We use 300 because it is how much we move over a second and it feels good even though the half of 400 is 200
		if (keyCode == GLFW_KEY_W) {
			object2.move(0, 300);
		} else if (keyCode == GLFW_KEY_S) {
			object2.move(0, -300);
		}

		if (keyCode == GLFW_KEY_A) {
			object2.move(-300, 0);
		} else if (keyCode == GLFW_KEY_D) {
			object2.move(300, 0);
		}

	}

}
