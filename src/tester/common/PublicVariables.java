package tester.common;

public class PublicVariables {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 800;
	public static final String WINDOW_NAME = "LWJGL Test";

	public static final boolean FULLSCREEN = false;

}
