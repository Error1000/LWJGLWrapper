package mainWrapper.renderEgnine.Objects;

import mainWrapper.renderEgnine.Material.Shader;
import mainWrapper.renderEgnine.Material.Texture;
import mainWrapper.renderEgnine.Shape.Shape;

public class TexturedObject extends DrawableObject {

	private final Texture objectTexture;

	public TexturedObject(Shape objectShape, Texture objectTexture, Shader shader) {
		super(objectShape, shader);
		this.objectTexture = objectTexture;

	}

	@Override
	public final void renderOtherStuff() {

		objectTexture.bindTexture();
	}
}
