package mainWrapper.renderEgnine.Objects;

import org.joml.Matrix4f;
import org.joml.Vector2f;

import mainWrapper.wrapper.Window.Camera;
import mainWrapper.wrapper.Window.Window;
import mainWrapper.renderEgnine.Material.Shader;
import mainWrapper.renderEgnine.Shape.RectShape;
import mainWrapper.renderEgnine.Shape.Shape;

public class DrawableObject {

	protected Shape objectShape;

	// Location
	protected Vector2f pos;

	// This is the thingy that let's opengl know how to show this object
	protected final Shader objectShader;

	public DrawableObject(Shape objectShape, Shader shader) {

		if (objectShape == null) {
			this.objectShape = new RectShape();
			System.err.println("Shape not defined, using default rectangle shape");
		} else {
			this.objectShape = objectShape;
		}

		this.objectShader = shader;

		if (shader == null) {
			System.err.println("Shader is null,can't use render function until it's going to be set!");
		}
		this.pos = new Vector2f(0, 0);

	}

	public DrawableObject(Shape objectShape, Shader shader, Vector2f pos) {
		this(objectShape, shader);
		setPosition(pos.x, pos.y);
	}
	
	public void render() {
		if (objectShader == null)
			return;

		if (Camera.isObjectOutsideCameraView(getPosition(), getSize()))
			return;
		// Load the shader so that when we need to use a shader we will use this one
		objectShader.bindShader();

		//This will be overridden by the inheriting class and it will bind the texture/animation/wathever..
		renderOtherStuff();

		// Set the projection matrix to the camera matrix + our matrix
		objectShader.setFloatMatrixUniform("projection", Camera.getProjection().mul(objectShape.getProjection()));

		// Render the shape using the shader, which in this case will put the texture
		// onto the shape
		objectShape.renderShape();

	}

	// This is used by other classes to bind the texture that we will use
	protected void renderOtherStuff() {}

	public void setPosition(float x, float y) {
		pos.set(x, y);
		// The division is just to translate between opengl coordinates and game world
		// coordinates (the camera translation is automatically applied by the projection matrix to trasnlate between game world coordonates and window coordonates)

		Vector2f openglPos = new Vector2f(x/(Window.getWidth()/2), y/(Window.getHeight()/2));
		// Note: the 0 is the z coordonate, because i am obligated to use z when i'm
		// using joml as the matrices library
		objectShape.setProjection(objectShape.getProjection().setTranslation(openglPos.x, openglPos.y, 0));

	}

	public final Vector2f getPosition() {
		return pos;
	}

	public final Matrix4f getProjection() {
		return objectShape.getProjection();
	}

	public void setSize(float sizeX, float sizeY) {
		// We use the shape setSize because it's more fitting for the shape to set the
		// size and more redundant than the drawable object
		objectShape.setSize(sizeX, sizeY);
	}

	public final Vector2f getSize() {
		return objectShape.getSize();
	}

	public final Shape getShape() {
		return objectShape;
	}

	public final Shader getShader() {
		return objectShader;
	}
	

	public void move(float xDistance, float yDistance) {
		// We multiply by delta to keep the speed constant independent of the frame rate
		float dxDistance = (float) (xDistance * Window.getDelta());
		float dyDistance = (float) (yDistance * Window.getDelta());
		// This will set the position of the player to the current position + how much
		// we want to move
		setPosition(pos.x + dxDistance, pos.y + dyDistance);
	}
	



}
