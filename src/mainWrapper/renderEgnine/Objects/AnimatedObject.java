package mainWrapper.renderEgnine.Objects;

import mainWrapper.renderEgnine.Material.Animation;
import mainWrapper.renderEgnine.Material.Shader;
import mainWrapper.renderEgnine.Shape.Shape;

public class AnimatedObject extends DrawableObject {

	private final Animation objectAnim;

	public AnimatedObject(Shape objectShape, Animation objectAnim, Shader shader) {
		super(objectShape, shader);
		this.objectAnim = objectAnim;

	}

	@Override
	public final void renderOtherStuff() {

		if (objectAnim != null)
			objectAnim.bindAnimation();
	}

}
