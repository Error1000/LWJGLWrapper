package mainWrapper.renderEgnine.Utils;


import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL15.*;


public class VBO {


    private final int id;

    
    public VBO() {
        id = glGenBuffers();
    }


    public final void bind(int target) {
        glBindBuffer(target, id);
    }


    public final void uploadData(int target, FloatBuffer data, int usage) {
        glBufferData(target, data, usage);
    }


    public final void uploadData(int target, long size, int usage) {
        glBufferData(target, size, usage);
    }

 

    public final void uploadSubData(int target, long offset, FloatBuffer data) {
        glBufferSubData(target, offset, data);
    }


    public final void uploadData(int target, IntBuffer data, int usage) {
        glBufferData(target, data, usage);
    }

  
    public final void delete() {
        glDeleteBuffers(id);
    }


    public final int getID() {
        return id;
    }

}