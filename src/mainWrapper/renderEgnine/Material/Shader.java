package mainWrapper.renderEgnine.Material;

import static org.lwjgl.opengl.GL20.*;

import java.nio.FloatBuffer;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;

import mainWrapper.wrapper.common.Utils.EngineUtils;

public class Shader {

	private final int programId;// we need and id for the program within a file since the program is going to be
	                           // written in another language we needs to parse it so we needs to identify it
	private final int vsId;// Vertex shader (manipulates vertices)
	private final int fsId;// Fragment shader(manipulates fragments of textures)

	public Shader(String vertexShader, String fragmentShader) {
		programId = glCreateProgram();

		// Load the vertex shader

		// We allocate space for the shaders and and get it's id and set it to vsId
		vsId = glCreateShader(GL_VERTEX_SHADER);

		// We set the vertex shader that has the id of vsId to what we read from the file
		glShaderSource(vsId, vertexShader);

		// Now we compile the source attached to the shader with the id vsId
		glCompileShader(vsId);

		// Check for errors when compiling the shader
		if (glGetShaderi(vsId, GL_COMPILE_STATUS) != 1) {
			System.err.println("Failed to compile vertex shader!");
			System.err.println(glGetShaderInfoLog(vsId));
			EngineUtils.exit();
		}

		// Load the fragment shader

		// We allocate space for the shaders and and get it's id and set it to fsId
		fsId = glCreateShader(GL_FRAGMENT_SHADER);

		// We set the vertex shader that has the id of fsId to what we read from the file
		glShaderSource(fsId, fragmentShader);

		// Now we compile the source attached to the shader with the id fsId
		glCompileShader(fsId);

		// Check for errors when compiling the shader
		if (glGetShaderi(fsId, GL_COMPILE_STATUS) != 1) {
			System.err.println("Failed to compile fragment shader!");
			System.err.println(glGetShaderInfoLog(fsId));
			EngineUtils.exit();
		}

		// Assign the two unique shader ids to one unique program id
		glAttachShader(programId, vsId);
		glAttachShader(programId, fsId);

		// This will pass our vertices array to the shader program and let it know that the vertices are 
		glBindAttribLocation(programId, 0, "vertices");
		glBindAttribLocation(programId, 1, "tex_coord");

		// Finally we link the vertexShader and the fragmentShader together so that tey
		// can talk and give the vertex shader the vertices array
		glLinkProgram(programId);

		// And we check for errors during the linking process, in case something went
		// wrong and one shader tried to communicate in some weird way with the other or
		// there are some communicating problems
		if (glGetProgrami(programId, GL_LINK_STATUS) != 1) {
			System.err.println("Failed to link shaders!");
			System.err.println(glGetProgramInfoLog(programId));
			EngineUtils.exit();
		}

		// Now we validate that everything went okay
		glValidateProgram(programId);

		// And now, just in case one final check just to make sure, you know, you can
		// never be sure in programming
		// This will check if somehow linking and compiling went okay but we couldn't
		// validate the program so this is in case some stuff is wrong but successfully
		// compiled
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) != 1) {
			System.err.println("Failed to validate shaders!");
			System.err.println(glGetProgramInfoLog(programId));
			EngineUtils.exit();
		}

	}

	// This will set a "uniform" variable in the shader
	public void setUniform(String name, int value) {
		// This will get the variable's location in the shader
		int uniformLoc = glGetUniformLocation(programId, name);

		// This will test if we could actually find the variable in the shader code
		if (uniformLoc != -1) {
			// This will actually set the "uniform" variable in the shader code
			glUniform1i(uniformLoc, value);
		} else {
			System.err.println("Couldn't find uniform: " + name + " in shader:" + programId + "!");
			EngineUtils.exit();
		}
	}

	// This will set a "uniform" variable in the shader
	public void setUniform(String name, float value) {
		// This will get the variable's location in the shader
		int uniformLoc = glGetUniformLocation(programId, name);

		// This will test if we could actually find the variable in the shader code
		if (uniformLoc != -1) {
			// This will actually set the "uniform" variable in the shader code
			glUniform1f(uniformLoc, value);
		} else {
			System.err.println("Couldn't find uniform: " + name + " in shader: " + programId + "!");
			EngineUtils.exit();
		}
	}

	// This will set a "uniform" variable in the shader
	public void setFloatMatrixUniform(String name, Matrix4f value) {
		// This will get the variable's location in the shader
		int uniformLoc = glGetUniformLocation(programId, name);

		FloatBuffer buffer = BufferUtils.createFloatBuffer(4 * 4);
		value.get(buffer);

		// This will test if we could actually find the variable in the shader code
		if (uniformLoc != -1) {
			// This will actually set the "uniform" variable in the shader code
			glUniformMatrix4fv(uniformLoc, false, buffer);
		} else {
			System.err.println("Couldn't find uniform: " + name + " in shader: " + programId + "!");
			EngineUtils.exit();
		}
	}

	public void bindShader() {
		// This will set the current shader to work with so that if opengl ever needs to
		// use a shader it will use this one
		glUseProgram(programId);
	}

	public final void deleteShader() {
		glDeleteProgram(programId);
	}
}
