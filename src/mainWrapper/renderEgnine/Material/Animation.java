package mainWrapper.renderEgnine.Material;

import mainWrapper.wrapper.Window.Window;

public class Animation {

	private final Texture[] frames;
	private int currentFrame = 0;

	private double elapsedTime;
	private double currentTime;
	private double lastTime;
	private final double AnimationFps;

	public Animation(int AnimFps, Texture[] frames) {
		this.currentFrame = 0;
		this.currentTime = 0;
		this.lastTime = Window.getCurrentTime();
		this.AnimationFps = AnimFps;
		this.elapsedTime = AnimFps - 1.0 / AnimFps;

		this.frames = frames;

	}

	public Animation(int amountOfFrames, int AnimFps, String filename) {
		this(AnimFps, prepareTexureArray(filename, amountOfFrames));

	}

	private final static Texture[] prepareTexureArray(String filename, int amountOfFrames) {
		Texture[] frames = new Texture[amountOfFrames];
		for (int i = 0; i < amountOfFrames; i++)
			frames[i] = new Texture(filename + i + ".png");

		return frames;
	}

	public void bindAnimation() {
		this.currentTime = Window.getCurrentTime();
		this.elapsedTime += this.currentTime - this.lastTime;

		if (elapsedTime >= this.AnimationFps) {
			elapsedTime -= 1.0 / this.AnimationFps;
			this.currentFrame++;
		}

		if (this.currentFrame >= frames.length)
			this.currentFrame = 0;

		this.lastTime = this.currentTime;

		this.frames[this.currentFrame].bindTexture();

	}

	public int getWidth() {
		return frames[0].getWidth();
	}

	public int getHeight() {
		return frames[0].getHeight();
	}
}
