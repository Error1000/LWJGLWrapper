package mainWrapper.renderEgnine.Material;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

public class Texture {

	private int id;
	private int width;
	private int height;

	public Texture(String filename) {
		this(readImage(filename));
	}

	private static final BufferedImage readImage(String filename) {
		BufferedImage i = null;
		try {
			i = ImageIO.read(new File(filename));
		} catch (IOException e) {
			System.err.println("Couldn't load texture: " + filename + "!");
			e.printStackTrace();
		}
		return i;
	}

	public Texture(BufferedImage img) {
		height = img.getHeight();
		width = img.getWidth();

		ByteBuffer image;

		int width = img.getWidth();
		int height = img.getHeight();

		int[] raw_pixels = new int[width * height];

		raw_pixels = img.getRGB(0, 0, width, height, null, 0, width);

		// Note 4 is the number of channels(red , green , blue and alpha)
		image = BufferUtils.createByteBuffer(width * height * 4);

		// Transform the int[] array into a buffer, for LAZY OPENGL THAT WON'T DO IT
		// ITSELF!!
		for (int i = 0; i < height; i++) {

			for (int j = 0; j < width; j++) {

				int pixel = raw_pixels[i * width + j];
				// Transfer the channels and stuff from the pixels array to the buffer
				image.put((byte) ((pixel >> 16) & 0xFF)); // RED
				image.put((byte) ((pixel >> 8) & 0xFF)); // GREEN
				image.put((byte) (pixel & 0xFF)); // BLUE
				image.put((byte) ((pixel >> 24) & 0xFF)); // ALPHA

			}

		}

		// This will prepare the buffer to be read from, for some weird reason
		image.flip();

		// Tell opengl to generate a unique id for this image
		id = glGenTextures();

		// Set the current texture that we are working on to be the texture wit the id
		// we jsut generated
		glBindTexture(GL_TEXTURE_2D, id);

		// Load the image into opengl
		// Prepare opengl to load the image by telling it some stuff about the image
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		// Actually load the image(byte buffer)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);

	}
	


	public void bindTexture() {

		// Set the sampler texture that we are working with, so that whenever something
		// that needs a texture from this sampler will shown this texture
		glBindTexture(GL_TEXTURE_2D, id);
		// This will set the "sample0"(texture location 0) to the current texture, this
		// will be used by the shader
		glActiveTexture(GL_TEXTURE0);

	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

}
