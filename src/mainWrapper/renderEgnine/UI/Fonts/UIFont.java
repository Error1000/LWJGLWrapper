package mainWrapper.renderEgnine.UI.Fonts;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import mainWrapper.renderEgnine.Material.Shader;
import mainWrapper.wrapper.Window.Window;
import mainWrapper.wrapper.common.Utils.EngineUtils;


public class UIFont {

    
    private final Map<Character, UIFontGlyph> glyphsMap;
    private final Shader fontShader;
    
public UIFont(Font font, Shader sh, float quality, boolean useAntiAlias) {
	
    glyphsMap = new HashMap<>();
    fontShader = sh;
    if(quality < 1) {System.err.println("Font quality(rendering size), is to small, defaulting to smallest quality(size of font) of 1!!"); quality = 1;}

    
    font = font.deriveFont(Font.PLAIN, quality);


    /* Create image for the standard chars, again we omit ASCII 0 to 31
     * because they are just control codes */
    for (int i = 32; i < 256; i++) {
        if (i == 127) {
            /* ASCII 127 is the DEL control code, so we can skip it */
            continue;
        }
        char c = (char) i;
        BufferedImage charImage = createCharGlyph(font, c, useAntiAlias);
        if (charImage == null) {
            /* If char image is null that font does not contain that char */
            continue;
        }

        int charWidth = charImage.getWidth();
        int charHeight = charImage.getHeight();

        /* Create glyph and draw char on image */
        UIFontGlyph ch = new UIFontGlyph(charWidth, charHeight, charImage);
        glyphsMap.put(c, ch);
        
    }


}
	

public UIFont(Font font, Shader s) {
	this(font, s, Math.round(((Window.getWidth() + Window.getHeight())/2)/11.43f), true);
}

public UIFont(Font font, Shader s, boolean useAntiAlias) {
	this(font, s, Math.round(((Window.getWidth() + Window.getHeight())/2)/11.43f), useAntiAlias);
}



public UIFont(InputStream is, Shader s) throws FontFormatException, IOException {
	this(Font.createFont(Font.TRUETYPE_FONT, is), s);
}

public UIFont(InputStream is, Shader s, float quality, boolean useAntiAlias) throws FontFormatException, IOException {
	this(Font.createFont(Font.TRUETYPE_FONT, is), s, quality, useAntiAlias);
}


public UIFont(InputStream is, Shader s, boolean useAntiAlias) throws FontFormatException, IOException {
	this(Font.createFont(Font.TRUETYPE_FONT, is), s, ((Window.getWidth() + Window.getHeight())/2) /11, useAntiAlias);
}



public UIFont(String filename, Shader s) throws FontFormatException, IOException {
	this(new FileInputStream(filename), s);
}

public UIFont(String filename, Shader s, float quality, boolean useAntiAlias) throws FontFormatException, IOException {
	this(new FileInputStream(filename), s, quality, useAntiAlias);
}


public UIFont(String filename, Shader s, boolean useAntiAlias) throws FontFormatException, IOException {
	this(new FileInputStream(filename), s, ((Window.getWidth() + Window.getHeight())/2) /11, useAntiAlias);
}

private static final BufferedImage createCharGlyph(Font font, char c, boolean useAntiAlias) {
	  /* Creating temporary image to extract character size */
	  BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
	  Graphics2D g = image.createGraphics();
	  if (useAntiAlias)g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	  g.setFont(font);
	  FontMetrics metrics = g.getFontMetrics();
	  g.dispose();

	  /* Get char charWidth and charHeight */
	  int charWidth = metrics.charWidth(c);
	  int charHeight = metrics.getHeight();

	  /* Check if charWidth is 0 */
	  if (charWidth == 0) {
	      System.err.println("Charachter width is 0!");
	      EngineUtils.exit();
	  }

	  /* Create glyph for holding the char */
	  image = new BufferedImage(charWidth, charHeight, BufferedImage.TYPE_INT_ARGB);
	  g = image.createGraphics();
	  if (useAntiAlias)g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	  g.setFont(font);
	  g.setPaint(Color.WHITE);
	  g.drawString(String.valueOf(c), 0, metrics.getAscent());
	  g.dispose();
	  
	  return image;
	  
   }




public final int getWith(String text) {
    int width = 0;
    int lineWidth = 0;
    for (int i = 0; i < text.length(); i++) {
        char c = text.charAt(i);
        if (c == '\n') {
            /* Line end, set width to maximum from line width and stored
             * width */
            width = Math.max(width, lineWidth);
            lineWidth = 0;
            continue;
        }
        if (c == '\r') {
            /* Carriage return, just skip it */
            continue;
        }
        UIFontGlyph g = glyphsMap.get(c);
        lineWidth += g.width;
    }
    width = Math.max(width, lineWidth);
return width;
}


public final int getHeight(String text) {
    int height = 0;
    int lineHeight = 0;
    for (int i = 0; i < text.length(); i++) {
        char c = text.charAt(i);
        if (c == '\n') {
            /* Line end, add line height to stored height */
            height += lineHeight;
            lineHeight = 0;
            continue;
        }
        if (c == '\r') {
            /* Carriage return, just skip it */
            continue;
        }
        UIFontGlyph g = glyphsMap.get(c);
        lineHeight = ((lineHeight > g.height) ? lineHeight : g.height);
    }
    
    height += lineHeight;
    return height;
}


public final UIFontGlyph getGlyphOfChar(Character c) {
	return glyphsMap.get(c);
}


public final Shader getShader() {
	return fontShader;
}


}
