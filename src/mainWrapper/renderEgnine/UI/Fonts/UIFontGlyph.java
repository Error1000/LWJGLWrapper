package mainWrapper.renderEgnine.UI.Fonts;

import java.awt.image.BufferedImage;

public class UIFontGlyph {
	
	   public final int width;
	   public final int height;
	   public final BufferedImage characterImg;
	   
public UIFontGlyph(int width, int height, BufferedImage characterImg) {
	        this.width = width;
	        this.height = height;
	        this.characterImg = characterImg;

}

}
