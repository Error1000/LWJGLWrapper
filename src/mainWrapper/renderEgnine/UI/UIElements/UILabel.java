package mainWrapper.renderEgnine.UI.UIElements;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.joml.Vector2f;

import mainWrapper.renderEgnine.Material.Shader;
import mainWrapper.renderEgnine.Material.Texture;
import mainWrapper.renderEgnine.Shape.RectShape;
import mainWrapper.renderEgnine.UI.Fonts.UIFont;
import mainWrapper.renderEgnine.UI.Fonts.UIFontGlyph;



public class UILabel extends UIElement{

	private UIFont labelFont;
	private Texture textTexture;
	private String labelContent;
	
	private int textWidth, textHeight;

	public UILabel(String content, UIFont font){
	  super(new RectShape(), font.getShader());
	  if(content == "") {System.err.println("The text can't be nothing(\"\"), defaulting to space(\" \")!"); content = " ";}
	  labelFont = font;
	  labelContent = content;
	  
	  textWidth = labelFont.getWith(labelContent);
	  textHeight = labelFont.getHeight(labelContent);
	  
	  BufferedImage text =  new BufferedImage(textWidth, textHeight, BufferedImage.TYPE_INT_ARGB);
	  Graphics2D g = text.createGraphics();
	  
	  int currentX = 0;
	  for(Character ch : labelContent.toCharArray()) {
		  
		UIFontGlyph charGlyph = labelFont.getGlyphOfChar(ch);
		
		g.drawImage(charGlyph.characterImg, currentX, 0, null);
		currentX += charGlyph.width;
		
	  }
	  
	  textTexture = new Texture(text);
	  objectShape.setSize(textWidth, textHeight);
	  
	  
	}
	
	public UILabel(String content, UIFont font, Shader s){
		  super(new RectShape(), s);
		  labelFont = font;
		  labelContent = content;
		 
		  textWidth = labelFont.getWith(labelContent);
		  textHeight = labelFont.getHeight(labelContent);
		  
		  BufferedImage text =  new BufferedImage(textWidth, textHeight, BufferedImage.TYPE_INT_ARGB);
		  Graphics2D g = text.createGraphics();
		  
		  int currentX = 0;
		  for(Character ch : labelContent.toCharArray()) {
			
			UIFontGlyph charGlyph = labelFont.getGlyphOfChar(ch);
			
			g.drawImage(charGlyph.characterImg, currentX, 0, null);
			currentX += charGlyph.width;
			
		  }
		  
		  textTexture = new Texture(text);
		  objectShape.setSize(textWidth, textHeight);
		  
		  
		}
	
	public UILabel(String content, UIFont font, Vector2f pos) {
		this(content, font);
		setPosition(pos.x, pos.y);
	}
	


	@Override
	public void renderOtherStuff(){
		textTexture.bindTexture();
	}
	
	
	
	public void setContent(String content) {

		textWidth = labelFont.getWith(content);
		textHeight = labelFont.getHeight(content);
		labelContent = content;

		
		BufferedImage text =  new BufferedImage(textWidth, textHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = text.createGraphics();
		  
		int currentX = 0;
		for(Character ch : labelContent.toCharArray()) {
			  
	       UIFontGlyph charGlyph = labelFont.getGlyphOfChar(ch);
			
		   g.drawImage(charGlyph.characterImg, currentX, 0, null);
		   currentX += charGlyph.width;
			
	    }
		  
		  textTexture = new Texture(text);
		  objectShape.setSize(textWidth, textHeight);
		  
		  
	}


	public void setFont(UIFont font) {
		textWidth = font.getWith(labelContent);
		textHeight = font.getHeight(labelContent);
		  
		
	    labelFont = font;
		
		BufferedImage text =  new BufferedImage(textWidth, textHeight, BufferedImage.TYPE_INT_ARGB);;
		Graphics2D g = text.createGraphics();
		  
		int currentX = 0;
		for(Character ch : labelContent.toCharArray()) {
			  
	       UIFontGlyph charGlyph = font.getGlyphOfChar(ch);
			
		   g.drawImage(charGlyph.characterImg, currentX, 0, null);
		   currentX += charGlyph.width;
			
	    }
		  
		  textTexture = new Texture(text);
		  objectShape.setSize(textWidth, textHeight);
		  
		  
	}
	

	public UIFont getFont() {
		return labelFont;
	}
	
	public String getContent() {
		return labelContent;
	}
	
	
	public int getTextWidth() {
		return textWidth;
	}
	
	public int getTextHeight() {
		return textHeight;
	}
}
