package mainWrapper.renderEgnine.UI.UIElements;



import mainWrapper.renderEgnine.Material.Shader;
import mainWrapper.renderEgnine.Objects.DrawableObject;
import mainWrapper.renderEgnine.Shape.Shape;


public abstract class UIElement extends DrawableObject{


	public UIElement(Shape objectShape, Shader shader) {
		super(objectShape, shader);
	}

@Override
public void render() {
	if (objectShader == null)
		return;

	// Load the shader so that when we need to use a shader we will use this one
	objectShader.bindShader();

	//This will be overridden by the inheriting class and it will bind the texture/animation/wathever..
	renderOtherStuff();

	// Set the projection matrix to the camera matrix + our matrix
	objectShader.setFloatMatrixUniform("projection", objectShape.getProjection());

	// Render the shape using the shader, which in this case will put the texture
	// onto the shape
	objectShape.renderShape();

}

	
}
