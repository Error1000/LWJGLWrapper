package mainWrapper.renderEgnine.UI;
import java.util.ArrayList;
import java.util.List;

import mainWrapper.renderEgnine.UI.UIElements.*;

public class UI{

	private List<UIElement> elements = new ArrayList<>();
	
	public void addElement(UIElement element) {
		elements.add(element);
	}
	

	
	public void setPosition(float x,float y) {
		for(UIElement e : elements)e.setPosition(x, y);
	}

	

	public void render() {
		for(UIElement e : elements)e.render();
		
	}
	
}
