package mainWrapper.renderEgnine.Shape;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.lwjgl.BufferUtils;

import mainWrapper.renderEgnine.Utils.VBO;
import mainWrapper.wrapper.Window.Window;

public class Shape {

	private final int draw_count;
	// Vertices to draw id
	private final VBO verticesVBO, indicesVBO, textureMapVBO;

	// This is used by the shader and tells it how to transform the shape(for
	// example we might want to rotate it and maybe translate it to it's position,
	// etc..)
	private Matrix4f ownProjection;
	// Scale
	private Vector2f size;

	public Shape(float[] vertices, float[] tex_map, int[] indices) {
		draw_count = indices.length;

		// Tell opengl to generate a buffer, give it and id and put the shape's vertices
		// in it
		
		verticesVBO = new VBO();

		// Let opengl know that here is an array/buffer with this id and make it
		// allocate the memory of the video card for it
		// Basically set the current buffer that we are working with
		verticesVBO.bind(GL_ARRAY_BUFFER);

		// Load the vertices data into the allocated memory and make it static so
		// basically tell opengl that we never need to change the data ever again,while this
		// won't throw an error accessing the data later from the gpu will be a lot slower and in general drawing will be faster
		verticesVBO.uploadData(GL_ARRAY_BUFFER, createBufferFromData(vertices), GL_STATIC_DRAW);

		// Tell opengl to generate a buffer, give it and id and put the shape's texture
		// coords in it
		indicesVBO = new VBO();

		// Load the vertices data into the allocated memory and make it static so
		// basically tell opengl that we never need to change the data ever again,while this
		// won't throw an error accessing the data later from the gpu will be a lot slower and in general drawing will be faster
		indicesVBO.bind(GL_ELEMENT_ARRAY_BUFFER);

		// Load the indices data into the allocated memory and make it static so
		// basically tell opengl that we never need to change the data ever again, this
		// will throw and error if we try to change the data later os this might be a
		// breaking point later in the code
		// HEAR THAT LATER CODING ME, THIS HERE MIGHT BE A PROBLEM!!!
		indicesVBO.uploadData(GL_ELEMENT_ARRAY_BUFFER, createBufferFromData(indices), GL_STATIC_DRAW);

		// Tell opengl to generate a buffer, give it and id and put the shape's texture
		// coords in it
		textureMapVBO = new VBO();

		// Let opengl know that here is an array/buffer with this id and make it
		// allocate the memory of the video card for it
		// Basically set the current buffer that we are working with
		textureMapVBO.bind(GL_ARRAY_BUFFER);

		// Load the texture coords data into the allocated memory and make it static so
		// basically tell opengl that we never need to change the data ever again, this
		// will throw and error if we try to change the data later os this might be a
		// breaking point later in the code
		// HEAR THAT LATER CODING ME, THIS HERE MIGHT BE A PROBLEM!!!
		textureMapVBO.uploadData(GL_ARRAY_BUFFER, createBufferFromData(tex_map), GL_STATIC_DRAW);

		ownProjection = new Matrix4f();
		size = new Vector2f(0, 0);

	}

	public void renderShape() {

		final int numberOfFloatsPerAttrib = 2;
		final int numberOfFloatsPerVertex = 2;
		// This will let us access the vavertex arrtibs from the shader
		// Note: We use 0 since this is the first atribute of a vertex(positions)

		glEnableVertexAttribArray(0);

		// This will let us access the vertex arrtibs from the shader
		// Note: We use 1 since this is the second atribute of a vertex(texture mappings)
		glEnableVertexAttribArray(1);

		// Set the current buffer to the vertices buffer so that we can work on it
		verticesVBO.bind(GL_ARRAY_BUFFER);

		// Set the shader's variable with id 0 to our vertices array from the buffer
		// We set it to the vertices array because it's the first attribute of a vertex since verticies can have many atrributes like color,position,etc.
		glVertexAttribPointer(0, numberOfFloatsPerAttrib, GL_FLOAT, false, 4 * numberOfFloatsPerVertex, 0);
		//4*numberOfFloatsPerVertex is how many bytes opengl has to go to get to the next vertex, notice we are using 4, that's because it's the sizeof(float)
		//finally we set the last argument to 0 since there is only one attribute per vertex in this array so in other words this array only contains positions for vertices

		// Set the current buffer to the texture coords buffer so that we can work on it
		textureMapVBO.bind(GL_ARRAY_BUFFER);

		// Set the shader's variable with id 1 to our texture mappings array from the buffer
		// We set it to the texture mappings array because it's the second attribute of a vertex since vertecies can have many atrributes like color,position,etc.
		glVertexAttribPointer(1, numberOfFloatsPerAttrib, GL_FLOAT, false, 4 * numberOfFloatsPerVertex, 0);
		//4*numberOfFloatsPerVertex is how many bytes opengl has to go to get to the next vertex, notice we are using 4, that's because it's the sizeof(float)
		//Finally we set the last argument to 0 since there is only one attribute per vertex in this array so in other words this array only contains positions for texture mappings

		//Now we bind the indices array, this time without putting it in a vertexattrib array because we don't need to access it in the shader
		// Set the current buffer to the indices buffer so that opengl can use it to draw this shape
		indicesVBO.bind(GL_ELEMENT_ARRAY_BUFFER);

		// Draw the vertices and the texture from the binded buffer and everything else
		// that needs to be drawn
		glDrawElements(GL_TRIANGLES, draw_count, GL_UNSIGNED_INT, 0);

		// Disable the ability to change the vertex arrtibs with id 0 from the shader
		glDisableVertexAttribArray(0);
		// Disable the ability to change the vertex arrtibs with id 1 from the shader
		glDisableVertexAttribArray(1);

	}

	private static final FloatBuffer createBufferFromData(float[] data) {
		// Transform the float[] array into a buffer, for LAZY OPENGL THAT WON'T DO IT
		// ITSELF!!
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);

		buffer.put(data);

		// This will prepare the buffer to be read from, for some weird reason
		buffer.flip();

		return buffer;
	}

	private static final IntBuffer createBufferFromData(int[] data) {
		// Transform the float[] array into a buffer, for LAZY OPENGL THAT WON'T DO IT
		// ITSELF!!
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);

		buffer.put(data);

		// This will prepare the buffer to be read from, for some weird reason
		buffer.flip();

		return buffer;
	}

	public final void deleteShape() {
		textureMapVBO.delete();
		verticesVBO.delete();
		indicesVBO.delete();
	}

	public final Matrix4f getProjection() {
		Matrix4f proj = ownProjection;
		return proj;
	}

	public final void setProjection(Matrix4f proj) {
		ownProjection = proj;
	}

	public void setSize(float sizeX, float sizeY) {

		// We use the size vector to resize accurately
		if (size.x != 0 && size.y != 0)
			ownProjection.scale(1 / size.x, 1 / size.y, 0);

		size.x = sizeX / Window.getWidth();
		size.y = sizeY / Window.getHeight();

		ownProjection.scale(size.x, size.y, 0);

	}
	

	public void resetProjection() {
		ownProjection = new Matrix4f();
		size = new Vector2f(0, 0);
	}

	public Vector2f getSize() {
		// Note: We don't multiply by half the window because the size is relative to
		// the entire window, not just half of it
		return new Vector2f(size.x * Window.getWidth(), size.y * Window.getHeight());
	}

}
