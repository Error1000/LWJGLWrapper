package mainWrapper.renderEgnine.Shape;

//This class is used for pre-made vertices that are easy to use
public class RectShape extends Shape {

	private static final float[] defaultVertices = new float[] {
			//Nr.
			//TOP RIGHT TRIANGLE
			-1f, 1f, //TOP LEFT          0
			1f, 1f, //TOP RIGHT         1
			1f, -1f, // BOTTOM RIGHT     2

			//BOTTOM LEFT TRIANGLE
			-1f, -1f // BOTTOM LEFT      3
	};

	//The texture vertices tells opengl which vertex/part of the photo corresponds to which vertex of the shape
	//Note: this has to have the same "layout" as the shape's vertices
	private static final float[] defaultTextureCoords = new float[] {
			//Nr.
			//TOP RIGHT TRIANGLE
			0, 0, //TOP LEFT VERTEX OF PHOTO         0
			1, 0, //TOP RIGHT VERTEX OF PHOTO        1
			1, 1, // BOTTOM RIGHT VERTEX OF PHOTO    2

			//BOTOM LEFT TRIANGLE
			0, 1 // BOTTOM LEFT VERTEX OF PHOTO 3

	};

	//The indices array basically tells opengl in which order to draw the vertices
	private static final int[] defaultIndices = new int[] {
			//TOP RIGHT TRIANGLE
			0, //TOP LEFT
			1, //TOP RIGTH
			3, //BOTTOM LEFT

			//BOTOM LEFT TRIANGLE
			1, //TOP RIGHT
			2, //BOTTOM RIGHT
			3 //BOTTOM LEFT

	};

	public RectShape() {
		super(defaultVertices, defaultTextureCoords, defaultIndices);
	}

}
