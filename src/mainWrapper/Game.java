package mainWrapper;

import mainWrapper.wrapper.MainWrapper;
import mainWrapper.wrapper.common.PublicVariables;
import mainWrapper.wrapper.common.Utils.MainClassUtils;

//This class has to be extended by the main class of the game and adds the necessary features that it needs as well as providing the starting function of the engine
public abstract class Game extends MainClassUtils {

	public abstract void setup();

	public abstract void render();

	public void keyReleased(int keyCode) {
	}

	public void keyPressed(int keyCode) {
	}

	public void keyIsDown(int keyCode) {
	}

	public void mousePressed(int mouseKey, float mouseX, float mouseY) {
	}

	public void mouseIsDown(int mouseKey, float x, float y) {
	}

	public void mouseReleased(int mouseKey, float mouseX, float mouseY) {
	}

	public void onExit() {
	}

	public static final void startWrapper() {

		// Set the mainClass so it can be used anywhere from here
		PublicVariables.mainClass = (Game) getMainClass();

		// Start the game engine and start up the window
		MainWrapper.runWrapper();

	}

	public static final void startWrapper(Game mainClass) {

		// Set the mainClass so it can be used anywhere from here
		PublicVariables.mainClass = mainClass;

		// Start the game engine and start up the window
		MainWrapper.runWrapper();

	}

	@SuppressWarnings("rawtypes")
	private static final Object getMainClass() {

		Object mainClass = null;
		try {
			StackTraceElement[] s = new Exception().getStackTrace();
			for (int i = 1; i < s.length; i++) {
				Class clst = Class.forName(s[i].getClassName());
				if (clst.getSuperclass().getSimpleName().equals("Game")) // this code is for interfaces -> ||
																			// Arrays.asList(clst.getInterfaces()).stream().filter(interfc
																			// ->
																			// interfc.getSimpleName().toLowerCase().equals("game")).count()
																			// > 0)
					try {
						mainClass = clst.newInstance();
					} catch (InstantiationException e) {
						System.err.println("There is something wrong with the main class!");
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						System.err.println("Couldn't access main class!");
						e.printStackTrace();
					}

			}

		} catch (ClassNotFoundException e) {
			System.err.println("Main class dosen't exist!");
			e.printStackTrace();
		}

		return mainClass;

	}

}