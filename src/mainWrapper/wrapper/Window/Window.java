package mainWrapper.wrapper.Window;

import static mainWrapper.wrapper.common.PublicVariables.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;


import org.lwjgl.glfw.GLFWErrorCallback;

import mainWrapper.wrapper.IO.KeyboardManager;
import mainWrapper.wrapper.IO.MouseManager;




public class Window {

	private static int width = 400;
	private static int height = 400;
	private static final int minWidth = 100;
	private static final int minHeight = 100;
	private static String name = "Game";
	private static boolean isFullscreen = false;
	private static boolean isInitialized = false;
	private static long window;

	// FPS stuff
	private static double lastTime = glfwGetTime();

	private static double deltaTime = 0;

	private static int fps = 0;
	private static int nbFrames = 0;
	private static double lt = glfwGetTime();

	public static final void setInitWidth(int width) {
		if (isInitialized) {
			System.err.println(
					"The window initial width can't be initialized after the game engine has started,but you can use the setWidth, setHeight and setTitle methods even after the engine has started!");
			return;
		}
		if (width >= minWidth) {
			Window.width = width;
		}else {
			System.err.println("The width of the window should be grater than or at least equal to " + minWidth + "!");
		}
		
	}

	public static final void setInitHeight(int height) {
		if (isInitialized) {
			System.err.println(
					"The window initial height can't be initialized after the game engine has started,but you can use the setWidth, setHeight and setTitle methods even after the engine has started!");
			return;
		}
		if (height >= minHeight) {
			Window.height = height;
		}else {
			System.err.println("The height of the window should be grater than or at least equal to " + minHeight + "!");
		}
		
	}

	public static final void setInitTitle(String title) {
		if (isInitialized) {
			System.err.println(
					"The window's initial title can't be initialized after the game engine has started,but you can use the setWidth, setHeight and setTitle methods even after the engine has started!");
			return;
		}
		Window.name = title;
	}

	public static final void startWindowFullscreen(boolean fullscreen) {
		if (isInitialized) {
			System.err.println(
					"The window can't be set to fullscren after the game engine has started,but you can use the setWidth, setHeight and setTitle methods even after the engine has started!");
			return;
		}
		Window.isFullscreen = fullscreen;
	}


	public static final void initWindow(int windowWidth, int windowHeight, String windowTitle) {
		if (isInitialized) {
			System.err.println(
					"The window can't be initialized after the game engine has started,but you can use the setWidth, setHeight and setTitle methods even after the engine has started!");
			return;
		}
		setInitWidth(windowWidth);
		setInitHeight(windowHeight);
		setInitTitle(windowTitle);

	}

	public static final void createWindow() {

		//Just to make sure that the what the game engine thinks is the window size is the actual window size
		//This is done just because it is used in calculations and it's a good idea for the two things to be the same  
		//Oh, btw this is here just in case someone sets the width and height of the window and also makes it full-screen at the same time
		if (isFullscreen) {
			width = SCREEN_WIDTH;
			height = SCREEN_HEIGHT;
		}

		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

		// Set window to be resizeable
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

		window = glfwCreateWindow(width, height, name, isFullscreen ? glfwGetPrimaryMonitor() : 0, 0);

		// Set minimum width and height the window can be
		glfwSetWindowSizeLimits(window, minWidth, minHeight, GLFW_DONT_CARE, GLFW_DONT_CARE);

		if (window == 0)
			throw new IllegalStateException("GLFW/LWJGL failed to create window!");

		glfwSetWindowPos(window, (SCREEN_WIDTH - width) / 2, (SCREEN_HEIGHT - height) / 2);

		// Finally we show the window
		glfwShowWindow(window);

		// Create "canvas"
		glfwMakeContextCurrent(window);

		// Set the window's callbacks
		setCallBacks();

		isInitialized = true;
	}

	public static final void startUpdateingWindow() {

		// Stuff to do with frame rate
		double currentTime = glfwGetTime();
		deltaTime = currentTime - lastTime;
		nbFrames++;
		if (currentTime - lt >= 1.0) {
			// If more that 1 second passed
			fps = nbFrames;
			nbFrames = 0;
			lt += 1.0;
		}

		lastTime = currentTime;

		// Pull new events / update buffer(context)
		glfwPollEvents();
		KeyboardManager.checkKeyboard();
		KeyboardManager.checkPredefinedKeybindings();
		MouseManager.checkMosue();
		
		// Clear last context("canvas"/frame)
		glClear(GL_COLOR_BUFFER_BIT);

	}

	public static final void stopUpdateingWindow() {
		// Spawn context("canvas")
		glfwSwapBuffers(window);
	}

	private static final void setCallBacks() {

		//Error callback
		glfwSetErrorCallback(new GLFWErrorCallback() {

			@Override
			public void invoke(int errorId, long description) {
				throw new IllegalStateException(GLFWErrorCallback.getDescription(description));
			}

		});

	

	}

	// Note: This function function not only sets what the engine thinks the height
	// of the window is, but also sets the actual window height
	public static final void setHeight(int height) {
		if (!isInitialized) {
			System.err.println(
					"Please start the engine first, but if you want to set the height before starting the engine use the init method of the window!");
			return;
		}
		if (height >= minHeight) {
			glfwSetWindowSize(window, width, height);
		} else {
			System.err.println("The height of the window should be grater than or at least equal to " + minHeight + "!");
		}

	}

	// Note: This function function not only sets what the engine thinks the width
	// of the window is, but also sets the actual window width
	public static final void setWidth(int width) {
		if (!isInitialized) {
			System.err.println(
					"Please start the engine first, but if you want to set the width before starting the engine use the init method of the window!");
			return;
		}

		if (width >= minWidth) {
			glfwSetWindowSize(window, width, height);
		} else {
			System.err.println("The width of the window should be grater than or at least equal to " + minWidth + "!");
		}
	}

	public static final int getWidth() {
		return width;
	}

	public static final int getHeight() {
		return height;
	}

	public static final void setTitle(String title) {
		if (!isInitialized) {
			System.err.println(
					"Please start the engine first, but if you want to set the title of the window before starting the engine use the init method of the window!");
			return;
		}

		name = title;
		glfwSetWindowTitle(window, name);
	}

	public static final String getTitle() {
		return name;
	}

	public static final long getWindow() {
		return window;
	}

	public static final double getDelta() {
		return deltaTime;
	}

	public static final int getFPS() {
		return fps;
	}

	public static final double getCurrentTime() {
		return glfwGetTime();
	}

}
