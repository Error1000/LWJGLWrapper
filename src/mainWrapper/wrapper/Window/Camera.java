package mainWrapper.wrapper.Window;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

import mainWrapper.renderEgnine.Objects.DrawableObject;

public class Camera {

	private static DrawableObject followedObject;
	private static boolean following = false;

	private static Vector2f position = new Vector2f(0, 0);

	// Base projection of all stuff
	private static Matrix4f projection = new Matrix4f()
			.setOrtho2D(-Window.getWidth() / 2, Window.getWidth() / 2, -Window.getHeight() / 2, Window.getHeight() / 2)
			.scale(Window.getWidth() / 2, Window.getHeight() / 2, 0)
			.setTranslation(new Vector3f(0, 0, 0));


	public static final void setProjection(int width, int height) {
		projection = new Matrix4f()
		.setOrtho2D(-width / 2, width / 2, -height / 2, height / 2)
		.scale(width/2, height/2,0)
		.setTranslation(new Vector3f(0, 0, 0));
		
	}

	// This will be called by the main engine to update the pos of the camera
	public static final void updateCamera() {
		final int recognitionRadius = 5;
		final float lerp = 2;
		projection.setTranslation(new Vector3f(-(position.x / (Window.getWidth() / 2)), -(position.y / (Window.getHeight() / 2)), 0));

		if (followedObject != null)
			lerpToTargetIfOutsideWindow(followedObject.getPosition(), recognitionRadius, lerp);

	}

	public static final void setPosition(Vector2f pos) {
		position = pos;
	}

	public static Vector2f getPosition() {
		return position;
	}

	public static final Matrix4f getProjection() {
		Matrix4f proj = new Matrix4f(projection);

		return proj;

	}

	public static final void setFollowingObject(DrawableObject o) {
		followedObject = o;
	}
	
	public static final DrawableObject getFollowingObject() {
		return followedObject;
	}

	private static final void lerpToTarget(Vector2f target, float lerp) {
		if (target == null)
			return;

		Vector2f camPos = new Vector2f(Camera.getPosition().x, Camera.getPosition().y);

		// Multiply by delta to adjust speed for frame rate
		camPos.x += (target.x - camPos.x) * lerp * Window.getDelta();
		camPos.y += (target.y - camPos.y) * lerp * Window.getDelta();

		Camera.setPosition(camPos);

	}

	public static final boolean isObjectOutsideCameraView(Vector2f pos, Vector2f size) {
		boolean outside = false;

        Vector2f relativePos = getPositionRelativeToWindow(pos);
        if (relativePos.x-size.x/2 > Window.getWidth() / 2
				|| relativePos.x+size.x/2 < -(Window.getWidth() / 2)) {

           outside = true;

		}

       	if (relativePos.y-size.y/2 > Window.getHeight() / 2
				|| relativePos.y + size.y/2 < -(Window.getHeight() / 2)) {

			outside = true;

		}

		return outside;
	}
	
	
	public static final Vector2f getPositionRelativeToWindow(Vector2f pos){
		return new Vector2f(pos.x- Camera.getPosition().x, pos.y-Camera.getPosition().y);
	}
	

	private static final void lerpToTargetIfOutsideWindow(Vector2f target, int stopRadius, float lerp) {

		if (!following)
			following = isObjectOutsideCameraView(target, new Vector2f(0, 0));
		if (isEqualWithMargOfErr(Camera.getPosition().x, target.x, stopRadius)
				&& isEqualWithMargOfErr(Camera.getPosition().y, target.y, stopRadius) && following) {

			following = false;
		}

		if (following)
			lerpToTarget(target, lerp);

	}

	private static final boolean isEqualWithMargOfErr(float first, float second, float margin) {
		return Math.abs(first - second) <= margin;
	}

}
