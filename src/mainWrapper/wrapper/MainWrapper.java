package mainWrapper.wrapper;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.GL;

import mainWrapper.wrapper.Window.Camera;
import mainWrapper.wrapper.Window.Window;
import mainWrapper.wrapper.common.PublicVariables;

//This class will be used by the main class and control everything
public final class MainWrapper {

	public static final void runWrapper() {

		// Initialize api's
		// This will run the glfwInit function and get it's return state if it returns
		// false then something has gone wrong so that's why there is an ! in front of
		// glfwInit()
		if (!glfwInit())
			throw new IllegalStateException("Failed to initialize GLFW!");

		// Init variables
		PublicVariables.init();

		// Code
		Window.createWindow();

		// Disable OPENGL vsync
		glfwSwapInterval(0);

		// Initialize gl
		GL.createCapabilities();

		// Tell opengl to load the tools needed to use 2d textures
		glEnable(GL_TEXTURE_2D);

		// Enable "transparency"
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		PublicVariables.mainClass.setup();
		
		System.gc();

		while (!glfwWindowShouldClose(Window.getWindow())) {

			Window.startUpdateingWindow();

			// Update the base projection/camera
			Camera.updateCamera();

			// Let the main class render something
			PublicVariables.mainClass.render();

			Window.stopUpdateingWindow();
		}

		// Call the exit function of the main class
		PublicVariables.mainClass.onExit();

		
		// Exit api's
		glfwTerminate();
		
		System.gc();

	}

}
