package mainWrapper.wrapper.common.Utils;

import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.opengl.GL11.glClearColor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import mainWrapper.wrapper.Window.Window;

public class MainClassUtils {

	public static int SCREEN_WIDTH = 0;
	public static int SCREEN_HEIGHT = 0;

	public static final void exit() {
		glfwSetWindowShouldClose(Window.getWindow(), true);
	}

	public static final void backgroundColor(int gray) {
		glClearColor(gray, gray, gray, 0);
	}

	public static final void backgroundColor(int r, int g, int b) {
		glClearColor(r, g, b, 0);
	}

	public static final String readTextFile(String filename) {
		StringBuilder sb = new StringBuilder();
		BufferedReader fileReader;

		try {
			fileReader = new BufferedReader(new FileReader(new File(filename)));

			String line;

			// This reads the file into the stringBuilder's "buffer"
			while ((line = fileReader.readLine()) != null)
				sb.append(line + '\n');

			fileReader.close();

		} catch (IOException e) {
			System.err.println("Couldn't load text file: " + filename);
			e.printStackTrace();
		}

		// This will return the "buffer" of the string builder
		return sb.toString();
	}

}
