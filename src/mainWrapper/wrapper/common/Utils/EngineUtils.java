package mainWrapper.wrapper.common.Utils;

import mainWrapper.wrapper.Window.Window;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;


public class EngineUtils {

	public static final float map(float value, float istart, float istop, float ostart, float ostop) {
		return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
	}


	public static final void exit() {
		glfwSetWindowShouldClose(Window.getWindow(), true);
	}

}
