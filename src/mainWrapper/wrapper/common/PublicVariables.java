package mainWrapper.wrapper.common;

import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;

import org.lwjgl.glfw.GLFWVidMode;

import mainWrapper.Game;
import mainWrapper.wrapper.common.Utils.MainClassUtils;


public class PublicVariables {

	public static int SCREEN_HEIGHT = 0;
	public static int SCREEN_WIDTH = 0;

	public static GLFWVidMode monitor;

	public static Game mainClass;


	public static final void init() {

		// Initialize some variables
		monitor = glfwGetVideoMode(glfwGetPrimaryMonitor());
		SCREEN_HEIGHT = monitor.height();
		SCREEN_WIDTH = monitor.width();

		MainClassUtils.SCREEN_HEIGHT = SCREEN_HEIGHT;
		MainClassUtils.SCREEN_WIDTH = SCREEN_WIDTH;

	}

}
