package mainWrapper.wrapper.IO;

import static org.lwjgl.glfw.GLFW.glfwGetMouseButton;

import java.nio.DoubleBuffer;

import org.lwjgl.BufferUtils;

import mainWrapper.wrapper.Window.Window;

import static mainWrapper.wrapper.common.PublicVariables.*;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.glfwGetCursorPos;

public class MouseManager {

	private static final boolean mouseButtonPressed[] = new boolean[] { false, false, false };
	private static DoubleBuffer xpos = BufferUtils.createDoubleBuffer(1);
	private static DoubleBuffer ypos = BufferUtils.createDoubleBuffer(1);
	private static float mouseX, mouseY;

	public static final void checkMosue() {

		for (int keyCode = 0; keyCode < mouseButtonPressed.length; keyCode++) {

			if (glfwGetMouseButton(Window.getWindow(), keyCode) == GLFW_TRUE) {
				xpos = BufferUtils.createDoubleBuffer(1);
				ypos = BufferUtils.createDoubleBuffer(1);
				glfwGetCursorPos(Window.getWindow(), xpos, ypos);
				mouseX = (float) (xpos.get() - Window.getWidth() / 2);
				mouseY = (float) -(ypos.get() - Window.getHeight() / 2);
			}

			if (glfwGetMouseButton(Window.getWindow(), keyCode) == GLFW_TRUE) {
				mainClass.mouseIsDown(keyCode, mouseX, mouseY);
			}

			if (!mouseButtonPressed[keyCode]) {

				if (glfwGetMouseButton(Window.getWindow(), keyCode) == GLFW_TRUE) {
					mainClass.mousePressed(keyCode, mouseX, mouseY);
					mouseButtonPressed[keyCode] = true;
				}

			} else {

				if (glfwGetMouseButton(Window.getWindow(), keyCode) == GLFW_FALSE) {
					mainClass.mouseReleased(keyCode, mouseX, mouseY);
					mouseButtonPressed[keyCode] = false;
				}

			}

		}

	}

}