package mainWrapper.wrapper.IO;

import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static mainWrapper.wrapper.common.PublicVariables.*;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.glfwGetKey;

import mainWrapper.wrapper.Window.Window;
import mainWrapper.wrapper.common.Utils.MainClassUtils;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LAST;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_0;

public class KeyboardManager {

	private static final boolean[] keyPressed = new boolean[GLFW_KEY_LAST];

	public static final void checkPredefinedKeybindings() {

		if (glfwGetKey(Window.getWindow(), GLFW_KEY_ESCAPE) == GLFW_TRUE) {
			MainClassUtils.exit();
		}

	}


	public static final void checkKeyboard() {


		for (int i = GLFW_KEY_0; i < GLFW_KEY_LAST; i++) {

			if (glfwGetKey(Window.getWindow(), i) == GLFW_TRUE) {
				// Call main's function
				mainClass.keyIsDown(i);
			}

			if (!keyPressed[i]) {

				if (glfwGetKey(Window.getWindow(), i) == GLFW_TRUE) {
					// Call main's function
					mainClass.keyPressed(i);
					keyPressed[i] = true;
				}

			} else {
				if (glfwGetKey(Window.getWindow(), i) == GLFW_FALSE) {
					// Call main's function
					mainClass.keyReleased(i);
					keyPressed[i] = false;
				}

			}

		}

	}

}